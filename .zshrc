# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

[[ ! -f ~/.zsh_alias ]] || source ~/.zsh_alias

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=9999
SAVEHIST=9999
setopt autocd beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "/home/${USER}/.zshrc"

autoload -Uz compinit
compinit

# my options
setopt histignorespace
setopt histignoredups
compdef -d vcsh # vcsh completion is broken for arbitrary git commands

autoload -Uz add-zsh-hook

function xterm_title_precmd () {
	print -Pn -- '\e]2;%n@%m %~\a'
	[[ "$TERM" == 'screen'* ]] && print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-}\e\\'
}

function xterm_title_preexec () {
	print -Pn -- '\e]2;%n@%m %~ %# ' && print -n -- "${(q)1}\a"
	[[ "$TERM" == 'screen'* ]] && { print -Pn -- '\e_\005{g}%n\005{-}@\005{m}%m\005{-} \005{B}%~\005{-} %# ' && print -n -- "${(q)1}\e\\"; }
}

if [[ "$TERM" == (alacritty*|gnome*|konsole*|putty*|rxvt*|screen*|tmux*|xterm*) ]]; then
	add-zsh-hook -Uz precmd xterm_title_precmd
	add-zsh-hook -Uz preexec xterm_title_preexec
fi

# plugins
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
source ~/.zsh/plugins/powerlevel10k/powerlevel10k.zsh-theme
source ~/.zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/plugins/command-not-found/command-not-found.plugin.zsh
# enable sourcing of optional plugins, if needed
[[ ! -f ~/.zsh_optional_plugins ]] || source ~/.zsh_optional_plugins
# sourcing syntax highlighting must be last, they say...
source ~/.zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# allow comments, but disable syntax highlighting for them, as readability may worsen
setopt interactivecomments
export ZSH_HIGHLIGHT_STYLES[comment]='none'
export PATH=~/bin:$PATH
export LS_COLORS="ow=01;33:di=01;33:mi=07;31"

if [[ -f /etc/bash_completion.d/azure-cli ]]; then
	autoload -U +X bashcompinit && bashcompinit
	source /etc/bash_completion.d/azure-cli
fi

export LANGUAGE=en_US.UTF-8
export LC_NUMERIC=en_US.UTF-8

bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
